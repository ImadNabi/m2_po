# Use an official Python runtime as the base image
FROM python:3.9-slim

# Set the working directory in the container to /app
WORKDIR /app

# Copy the current directory contents into the container at /app

COPY . /app
# Install the required packages

RUN pip install -r requirements.txt

#Expose port 8081

#to the host machine
ENV FLASK_APP=application.py

#Run the command to start the Flask app

EXPOSE 8081
# Run the command to start the Flask app
CMD ["flask", "run", "--host=0.0.0.0", "--port=8081"]