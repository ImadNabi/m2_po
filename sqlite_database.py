import sqlite3
import json

def row_to_dict(cursor: sqlite3.Cursor, row: sqlite3.Row) -> dict:
    data = {}
    for idx, col in enumerate(cursor.description):
        data[col[0]] = row[idx]
    return data


def get_database_connextion():
    con = sqlite3.connect("employee.db")
    con.row_factory = row_to_dict
    return con

def create_employee_table():
    con = get_database_connextion()
    cursor = con.cursor()
    query = """ CREATE TABLE IF NOT EXISTS employees(
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                firstName VARCHAR(15),
                lastName VARCHAR(15),
                emailId VARCHAR(25));"""
    cursor.execute(query)
    con.commit()
    cursor.close()
    con.close()


def list_employees():
    con = get_database_connextion()
    cursor = con.cursor()
    query = """ SELECT id, firstName, lastName, emailId
                FROM employees
                ORDER BY firstName, lastName ASC;
            """
    cursor.execute(query)
    result = cursor.fetchall()
    cursor.close()
    con.close()
    return result

def load_employee(employee_id):
    con = get_database_connextion()
    cursor = con.cursor()
    query = """ SELECT id, firstName, lastName, emailId
                FROM employees
                WHERE id = ?;
            """
    
    result = cursor.execute(query,(int(employee_id),))
    result = cursor.fetchone()
    cursor.close()
    con.close()
    return result

def add_employee(firstName, lastName, emailId):
    con = get_database_connextion()
    cursor = con.cursor()
    query = """ INSERT INTO employees(firstName, lastName, emailId)
                VALUES(?, ?, ?);
            """
    cursor.execute(query,(firstName, lastName, emailId))
    con.commit()
    cursor.close()
    con.close()

def delete_employee(employee_id):
    con = get_database_connextion()
    cursor = con.cursor()
    query = """ DELETE FROM employees
                WHERE id = ?;
            """
    
    cursor.execute(query,(int(employee_id),))
    con.commit()
    cursor.close()
    con.close()

def update_employee(employee_id, firstName, lastName, emailId):
    con = get_database_connextion()
    cursor = con.cursor()
    query = """ UPDATE employees
                SET firstName = ?, lastName = ?, emailId = ?
                WHERE id = ?; 
            """
    cursor.execute(query, (firstName, lastName, emailId, int(employee_id)))
    con.commit()
    cursor.close()
    con.close()
    

    