from flask import Flask, request, jsonify
from flask_cors import CORS
import sqlite_database
application =Flask(__name__)
application.secret_key = "password"
CORS(application)


@application.route("/api/v1/employees",  methods=['GET'])
def index():
    employees =  sqlite_database.list_employees()
    print(employees)
    return employees

@application.route("/api/v1/employees", methods=['POST'])
def add():
    request_data = request.get_json(force=True)
    print(request_data)
    firstName = request_data["firstName"]
    lastName = request_data["lastName"]
    emailId  = request_data["emailId"]
    
    sqlite_database.add_employee(firstName, lastName, emailId)
    
    return jsonify({'message': f'New employee added successfully'})

@application.route("/api/v1/employees/<int:id>", methods=["GET"])
def load_employee(id):
    
    return sqlite_database.load_employee(id)
    print("db is ok")
    

@application.route("/api/v1/employees/<int:id>", methods=["DELETE"])
def delete(id):
    
    sqlite_database.delete_employee(id)
    
    return jsonify({'message': f'Employee_id [{id}] deleted successfully'})

@application.route("/api/v1/employees/<int:id>", methods=["PUT"])
def update(id):
    request_data = request.get_json(force=True)
    firstName = request_data["firstName"]
    lastName = request_data["lastName"]
    emailId  = request_data["emailId"]
    
    sqlite_database.update_employee(id, firstName, lastName, emailId)
    
    return jsonify({'message': f'Employee_id [{id}] updated successfully'})

if __name__ == "__main__":
    sqlite_database.create_employee_table()
    application.run(port=8081) # http://localhost:5000